### Docker Compose - Monitoring:

Monitor all running containers on Host

- Grafana
- Prometheus
- Cadvisor
- AlertManager
- NodeExporter

        Grafana: http://localhost:3000 
        (user: admin, pass: admin)

        Prometheus: http://localhost:9090
